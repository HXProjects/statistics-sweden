# Statistics Sweden

React-redux app

Sweden statistics scb.se open api

## link [development](https://reverent-thompson-86963f.netlify.app/)

## link [production](https://gifted-villani-d62571.netlify.app/)

## Use YARN package manager!

## Available Scripts

In the project directory, you can run:

### `yarn dev`

Runs concurrently the app <br>
Open [https://localhost:9000](https://localhost:9000) for the app.<br>

### `yarn build`

Builds the app for production to the `build` folder.<br>
