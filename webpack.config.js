const webpack = require('webpack');
const path = require('path');
const DIST_DIR = path.resolve(__dirname, 'dist');

const APP_DIR = path.resolve(__dirname, 'src');
const BUILD_DIR = path.resolve(__dirname, 'dist');

process.env.NODE_ENV = 'development';

const config = {
  entry: APP_DIR + '/index.js',
  devServer: {
    contentBase: DIST_DIR,
    open: true,
    port: 9000,
    disableHostCheck: true,
    historyApiFallback: true,
    https: true,
    headers: { 'Access-Control-Allow-Origin': '*' },
  },
  output: {
    path: BUILD_DIR,
    filename: 'js/app.bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: ['babel-loader'],
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
};

module.exports = config;
