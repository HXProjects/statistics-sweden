import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import Root from './components/Root';
import * as serviceWorker from './serviceWorker';
import './utils/i18n';
import configureStore from './redux/configure-store';
import { Provider as ReduxProvider } from 'react-redux';

const store = configureStore();

ReactDOM.render(
  <ReduxProvider store={store}>
    <BrowserRouter>
      <Root />
    </BrowserRouter>
  </ReduxProvider>,
  document.getElementById('root')
);
serviceWorker.unregister();
