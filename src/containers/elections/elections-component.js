import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as electionActions from '../../redux/actions/election-actions';
import ElectionsData from '../../components/elections/elections';

const mapStateToProps = (state) => {
  return {
    elections: state.elections,
    loading: state.apiCallsInProgress > 0,
  };
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(electionActions, dispatch),
  };
}

export const ParliamentElectionsComponent = connect(
  mapStateToProps,
  mapDispatchToProps
)(ElectionsData);
