import React, { useEffect, useState, useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import grey from '@material-ui/core/colors/grey';
import green from '@material-ui/core/colors/green';
import SettingsIcon from '@material-ui/icons/Settings';
import Button from '@material-ui/core/Button';

import { Spinner } from '../spinner/linear-spinner';
import { YearsSettings } from '../election-settings/years-settings';
import { RegionSettings } from '../election-settings/region-settings';
import { getQuery } from '../../services/get-query';
import {
  getMaxValuePerYear,
  changeKeyToName,
} from '../../services/data-handler';
import { MaxValueGraph } from '../graph/max-value-graph';
import { InfoSCB } from './scb-info';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(() => ({
  panel_container: {
    minHeight: '625px',
    maxHeight: '625px',
    zIndex: '200',
    boxShadow: '0 0 10px rgba(0,0,0,0.2)',
    borderRadius: '5px',
    position: 'fixed',
    marginTop: '4%',
    right: '80px',
    maxWidth: '400px',
    minWidth: '400px',
  },
  panel: {
    justifySelf: 'center',
    display: 'grid',
    gridTemplateColumns: '3fr 2fr',
    gap: '10px',
    borderBottom: `solid,1px ${grey[100]}`,
    minHeight: '550px',
    maxHeight: '550px',
    backgroundColor: 'white',
    height: '80vh',
  },
  region_section: {
    margin: '200px',
  },
  settings_button: {
    position: 'fixed',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    top: '150px',
    right: '0px',
    width: '70px',
    height: '50px',
    backgroundColor: 'rgba(0,0,0,0.3)',
    borderRadius: '2px',
  },
  settings_icon: {
    color: grey[50],
  },
  unvisible: {
    display: 'none',
  },
  submit_button: {
    display: 'block',
    marginRight: '24px',
    marginLeft: '24px',
    marginTop: '25px',
    width: '350px',
    backgroundColor: green[500],
    fontWeight: 'bold',
  },
  submit_button_disabled: {
    display: 'block',
    marginRight: '24px',
    marginLeft: '24px',
    marginTop: '25px',
    width: '350px',
    backgroundColor: green[500],
    fontWeight: 'bold',
  },
  body: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap',
    margin: 20,
  },
  spinner: {
    marginTop: '45px',
  },
}));

const ElectionsData = (props) => {
  const { t } = useTranslation();
  const classes = useStyles();
  const settingsRef = useRef();
  const [blockedButton, setBlockedButton] = useState(false);
  const [options, setOptions] = useState({
    regions: ['00'],
    years: [],
  });
  const [yearsOption, setYearsOption] = useState(
    props.elections.availableYears
  );
  const [regionsOption, setRegionsOption] = useState(['00']);

  const [settingsPanelVisibility, setSettingsPanelVisibility] = useState(true);

  useEffect(() => {
    props.actions.loadParliamentData().catch((error) => {
      console.log('electiondatacomponent' + error);
    });
    props.actions.loadExistingYears().catch((error) => {
      console.log('available year error' + error);
    });
    props.actions.loadExistingRegions().catch((error) => {
      console.log('available region error' + error);
    });
  }, []);

  function handleSettingsButtonClick(event) {
    event.preventDefault();
    const panel = settingsRef.current;
    panel.className = settingsPanelVisibility
      ? classes.unvisible
      : classes.panel_container;
    setSettingsPanelVisibility(!settingsPanelVisibility);
  }

  function getChosenYears(years) {
    if (options.years.length == 0) {
      setOptions({ regions: options.regions, years: years });
    }

    years.length ? setBlockedButton(false) : setBlockedButton(true);
    setYearsOption(years);
  }

  function getChosenRegions(regions) {
    regions.length ? setBlockedButton(false) : setBlockedButton(true);
    setRegionsOption(regions);
  }

  function getChosenOptions() {
    setOptions({ years: yearsOption, regions: regionsOption });
    const que = getQuery({
      key: props.code,
      time: yearsOption,
      region: regionsOption,
    });
    props.actions.loadParliamentData(que);
  }

  const getDataForMaxGrapgh = () => {
    const arrayToHandle = props.elections.elections.data;
    const regions = props.elections.availableRegions;
    const result = getMaxValuePerYear(arrayToHandle);
    const list = changeKeyToName(result, regions);

    return list;
  };

  const disabled = blockedButton ? { disabled: true } : {};

  return (
    <>
      <div
        className={classes.settings_button}
        onClick={handleSettingsButtonClick}
      >
        <SettingsIcon className={classes.settings_icon} />
      </div>
      <div ref={settingsRef} className={classes.panel_container}>
        <div className={classes.panel}>
          <div>
            {props.elections.availableRegions && (
              <RegionSettings
                regions={props.elections.availableRegions}
                className={classes.region_section}
                sendData={getChosenRegions}
              />
            )}
          </div>
          <div>
            {props.elections.availableYears && (
              <YearsSettings
                years={props.elections.availableYears}
                className={classes.years_section}
                sendData={getChosenYears}
              />
            )}
          </div>
        </div>
        <Button
          variant="outlined"
          className={classes.submit_button}
          onClick={getChosenOptions}
          {...disabled}
        >
          {t('Set')}
        </Button>
      </div>
      <div className={classes.spinner}>
        {!props.elections.elections.data &&
          !props.elections.availableRegions && <Spinner />}
      </div>
      <div className={classes.body}>
        <InfoSCB info={props.topic} />
        {props.elections.elections.data && props.elections.availableRegions && (
          <MaxValueGraph regions={getDataForMaxGrapgh()} options={options} />
        )}
      </div>
    </>
  );
};

export default ElectionsData;
