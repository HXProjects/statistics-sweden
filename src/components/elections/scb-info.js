import React from 'react';
import { useTranslation } from 'react-i18next';

import { makeStyles } from '@material-ui/core/styles';
import { InfoVoting } from './election-pages-info';
const useStyles = makeStyles(() => ({
  root_info: {
    margin: '70px 45px',
    fontSize: '20px',
  },
}));

export const InfoSCB = ({ info }) => {
  const classes = useStyles();
  const { t } = useTranslation();

  const source = t('Source Statistics');
  const information = 'Statistics Sweden is statistics accessible via open Api';

  return (
    <div className={classes.root_info}>
      <h4>{source}</h4>
      <div>{t(information)}</div>
      <InfoVoting topic={info} />
    </div>
  );
};
