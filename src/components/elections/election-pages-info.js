import React from 'react';
import { useTranslation } from 'react-i18next';

export const InfoVoting = ({ topic }) => {
  const { t } = useTranslation();

  const information = `${t(
    'Here you can find voting rates in elections to'
  )} ${topic} ${t('by region')}. ${t('Year of election 1973 - 2018')}`;

  return <div>{information}</div>;
};
