import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import LanguageIcon from '@material-ui/icons/Language';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';

export const LanguageSelector = () => {
  const { i18n, t } = useTranslation();
  const [anchorEl, setAnchorEl] = useState();
  const defaultLang = 'en';
  const [actualLang, setActualLang] = useState(defaultLang);

  const open = !!anchorEl;
  const languages = {
    sv: 'Svenska',
    en: 'English',
  };

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const changeLanguage = (locale) => {
    i18n.changeLanguage(locale);
  };
  const clickOnLangOption = (event) => {
    setActualLang(`${event.currentTarget.dataset.lang}`);
    changeLanguage(`${event.currentTarget.dataset.lang}`);
    handleClose();
  };
  return (
    <div className="language-select-container">
      <IconButton
        aria-label={t('Language selector')}
        aria-controls="menu-appbar"
        aria-haspopup="true"
        onClick={handleMenu}
      >
        <LanguageIcon />
      </IconButton>
      <Menu
        id="menu-appbar"
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={open}
        onClose={handleClose}
      >
        {Object.keys(languages).map((id) => (
          <MenuItem
            key={id}
            selected={id === actualLang}
            data-lang={id}
            onClick={clickOnLangOption}
          >
            {languages[id]}
          </MenuItem>
        ))}
      </Menu>
    </div>
  );
};
