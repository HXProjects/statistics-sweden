import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import yellow from '@material-ui/core/colors/yellow';
import grey from '@material-ui/core/colors/grey';
import lightBlue from '@material-ui/core/colors/lightBlue';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import { ParliamentElectionsComponent } from '../../containers/elections/elections-component';
import * as apiKeys from '../../services/api-keys';

const side_nav_background = '../assets/images/side_nav_background.jpg';

function TabPanel({ children, value, index, ...props }) {
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...props}
    >
      {value === index && <div>{children}</div>}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function setIdTotab(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: grey[100],
    display: 'flex',
    height: '100%',
    overflow: 'hide',
  },
  tabs: {
    width: '20%',
    borderRight: `2px solid ${theme.palette.divider}`,
    backgroundImage: `url(${side_nav_background})`,
    backgroundSize: 'cover',
    fontWeight: 'bolder',
    backgroundRepeat: 'no-repeat',
  },
  yellowtab: {
    backgroundColor: yellow[400],
    margin: '70px 10px 10px 10px',
    fontWeight: 'bold',
    fontStretch: 'wider',
    borderRadius: '5px',
    alignSelf: 'center',
    width: '100%',
  },
  whitetab: {
    backgroundColor: grey[50],
    margin: '10px 10px 10px 10px',
    fontWeight: 'bold',
    fontStretch: 'wider',
    borderRadius: '5px',
    alignSelf: 'center',
    width: '100%',
  },
  bluetab: {
    backgroundColor: lightBlue[400],
    margin: '10px 10px 10px 10px',
    fontWeight: 'bold',
    borderRadius: '5px',
    fontStretch: 'wider',
    alignSelf: 'center',
    width: '100%',
  },
}));

export const VerticalTabs = () => {
  const { t } = useTranslation();

  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const riksdag = t('the Riksdag');
  const country = t('County Councils');
  const municipal = t('Municipal Councils');
  return (
    <div className={classes.root}>
      <Tabs
        orientation="vertical"
        indicatorColor="secondary"
        value={value}
        onChange={handleChange}
        className={classes.tabs}
      >
        <Tab
          className={classes.yellowtab}
          label={t('The Riksdag')}
          {...setIdTotab(0)}
        />
        <Tab
          className={classes.whitetab}
          label={t('County Council')}
          {...setIdTotab(1)}
        />
        <Tab
          className={classes.bluetab}
          label={t('Municipal Council')}
          {...setIdTotab(2)}
        />
      </Tabs>
      <TabPanel value={value} index={0}>
        <ParliamentElectionsComponent
          code={apiKeys.parliamentKey}
          topic={riksdag}
        />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <ParliamentElectionsComponent
          topic={country}
          code={apiKeys.countryCouncilKey}
        />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <ParliamentElectionsComponent
          topic={municipal}
          code={apiKeys.municipalCouncilKey}
        />
      </TabPanel>
    </div>
  );
};
