import React from 'react';
import { useTranslation } from 'react-i18next';
import { VerticalTabs } from '../election-side-nav/election-switch-tabs';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%',
  },
}));

export const ElectionsPage = ({ route }) => {
  const { t } = useTranslation();
  const classes = useStyles();
  return (
    <>
      <VerticalTabs className={classes.root} />
    </>
  );
};
