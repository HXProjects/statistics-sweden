import React from "react";

import { renderRoutes } from "react-router-config";

import routes from "../utils/routes";

const Root = ({ route }) => renderRoutes(routes);

export default Root;
