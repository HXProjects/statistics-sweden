import React from 'react';
import { renderRoutes } from 'react-router-config';
import { makeStyles } from '@material-ui/core/styles';

import { Header } from '../header/header';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%',
  },
}));

export const App = ({ route }) => {
  const classes = useStyles();

  return (
    <>
      <Header />

      <main className={classes.root}>
        {route && renderRoutes(route.routes)}
      </main>
    </>
  );
};
