import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import { makeStyles } from '@material-ui/core/styles';

import { RegionDataSection } from './regions-data-form';

const useStyles = makeStyles((theme) => ({
  region_settings: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    listStyle: 'none',
    padding: theme.spacing(0.5),
    margin: '10px',
    borderBottom: 'solid 1px grey',
  },
  region_list: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    listStyle: 'none',
    marginTop: '50px 10px 0px 10px',
    padding: theme.spacing(0.5),
  },
  unvisible: {
    display: 'none',
  },
}));

export const RegionSettings = ({ regions, sendData }) => {
  const { t } = useTranslation();
  const classes = useStyles();
  const [markedTempRegion, setMarkedTempRegion] = useState([]);
  const [value, setValue] = useState('state');
  const [regionVisibility, setRegionVisibility] = useState(false);

  const municipalities = 'municipalities';
  const country = 'state';
  const stateKey = ['00'];

  const handleChange = (event) => {
    const chosenValue = event.target.value;

    setRegionVisibility(chosenValue === municipalities);
    setValue(chosenValue);

    if (chosenValue === country) {
      sendData(stateKey);
    } else if (chosenValue === municipalities) {
      sendData(markedTempRegion);
    }
  };

  function getChosenRegion(regions) {
    regions.length ? setMarkedTempRegion(regions) : setMarkedTempRegion([]);
    if (regionVisibility) {
      regions.length ? sendData(regions) : sendData([]);
    }
  }

  return (
    <>
      <div className={classes.region_settings}>
        <FormControl component="fieldset">
          <FormLabel component="legend">Region:</FormLabel>
          <RadioGroup
            aria-label="region"
            name="region"
            value={value}
            onChange={handleChange}
          >
            <FormControlLabel
              value={country}
              control={<Radio color="default" />}
              label={t('State')}
            />
            <FormControlLabel
              value={municipalities}
              control={<Radio color="default" />}
              label={t('Municipalities')}
            />
          </RadioGroup>
        </FormControl>

        <div className={regionVisibility ? '' : classes.unvisible}>
          {regions && (
            <RegionDataSection
              regions={regions}
              sendRegions={getChosenRegion}
              className={classes.region_list}
            />
          )}
        </div>
      </div>
    </>
  );
};
