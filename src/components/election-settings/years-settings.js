import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';
import Checkbox from '@material-ui/core/Checkbox';
import indigo from '@material-ui/core/colors/indigo';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    listStyle: 'none',
    padding: theme.spacing(0.5),
    margin: 0,
  },
  markedChip: {
    margin: theme.spacing(0.4),
    background: indigo[400],
    color: indigo[50],
  },
  unmarkedChip: {
    margin: theme.spacing(0.4),
    background: indigo[100],
  },
  years_section: {
    borderLeft: 'solid 1px #ccc',
    justifyContent: 'center',
    fontWeight: 'bold',
  },
  chips_list: {
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap',
    margin: '5px 20px',
    color: 'primary',
  },
  focus: {
    width: '0',
    overflow: 'hidden',
    height: '0',
    opacity: '0',
  },
}));

export const YearsSettings = ({ years, sendData }) => {
  const { t } = useTranslation();
  const [allMarked, setAllMarked] = useState(true);

  const [chipData, setChipData] = useState(
    years.map((year) => {
      return {
        key: year,
        label: year,
        marked: true,
      };
    })
  );

  const classes = useStyles();

  useEffect(() => {
    sendData(handleChosenYears());
  }, []);

  const handleYearClick = (clickedYear) => (event) => {
    event.preventDefault();
    setChipData(
      chipData.map((chip) => {
        if (clickedYear.label == chip.label) {
          chip.marked = !chip.marked;
        }
        return chip;
      })
    );

    if (chipData.filter((chip) => chip.marked).length === 0) {
      setAllMarked(false);
    }

    if (chipData.filter((chip) => chip.marked).length === chipData.length) {
      setAllMarked(true);
    }
    sendData(handleChosenYears());
    document.getElementById('clear_focus').focus();
  };

  const handleChangeAll = (event) => {
    setAllMarked(event.target.checked);
    setChipData(
      chipData.map((chip) => {
        chip.marked = event.target.checked;

        return chip;
      })
    );
    sendData(handleChosenYears());
  };

  const handleChosenYears = () => {
    const chosenYears = chipData.filter((chip) => chip.marked);
    return chosenYears.map((chip) => chip.key);
  };

  return (
    <>
      <h4 id="head"> {t('Election year')} </h4>
      <div className={classes.years_section}>
        <Checkbox
          checked={allMarked}
          color="primary"
          onChange={handleChangeAll}
          inputProps={{ 'aria-label': 'primary checkbox' }}
        />
        <span>{allMarked ? t('Unmark all') : t('Mark all')}</span>
        <div className={classes.chips_list}>
          {chipData.map((data) => {
            return (
              <Chip
                size="small"
                key={data.key}
                label={data.label}
                onClick={handleYearClick(data)}
                data-name="year"
                className={
                  data.marked ? classes.markedChip : classes.unmarkedChip
                }
              />
            );
          })}
        </div>
        <button id="clear_focus" className={classes.focus} />
      </div>
    </>
  );
};

YearsSettings.propTypes = {
  years: PropTypes.array.isRequired,
  sendData: PropTypes.func.isRequired,
};
