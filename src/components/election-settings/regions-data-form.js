import React, { useState, useRef, useMemo, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import Zoom from '@material-ui/core/Zoom';
import Chip from '@material-ui/core/Chip';
import FormControl from '@material-ui/core/FormControl';
import { makeStyles } from '@material-ui/core/styles';
import green from '@material-ui/core/colors/green';
import indigo from '@material-ui/core/colors/indigo';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import * as _ from 'lodash';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    height: '100%',
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
    margin: '20px',
    maxWidth: '100%',
    maxHeight: '300px',
    overflowY: 'scroll',
    overflowX: 'hidden',
  },
  textInput: {
    display: 'inline',
    margin: theme.spacing(1),
    width: '75%',
    verticalAlign: 'bottom',
    flexWrap: 'no-wrap',
  },
  markedChip: {
    margin: theme.spacing(0.5),
    background: indigo[400],
    color: indigo[50],
  },
  unmarkedChip: {
    margin: theme.spacing(0.5),
    background: green[100],
  },
  unvisible: {
    display: 'none',
  },
}));

export const RegionDataSection = ({ regions, sendRegions }) => {
  const { t } = useTranslation();
  const classes = useStyles();
  const textEl = useRef();
  const { values, valueTexts } = regions;
  const totalRegions = valueTexts.length - 1;
  const [regionCounter, setRegionCounter] = useState({
    marked: 0,
    total: totalRegions,
  });

  const [loading, setLoading] = useState(true);
  const [municipalities, setMunicipalities] = useState(
    _.sortBy(getOnlyRegionsArray(), ['label'])
  );
  const [allMarked, setAllMarked] = useState(false);

  useEffect(() => {
    sendRegions(getMarkedRegions());
  }, []);

  function getMarkedCount() {
    return municipalities.filter((chip) => chip.marked).length;
  }
  function getMarkedRegions() {
    const dataToSend = municipalities
      .filter((chip) => chip.marked)
      .map((reg) => reg.key);
    return dataToSend;
  }

  function getOnlyRegionsArray() {
    const onlyRegionsValues = valueTexts.slice(1);
    return _.map(onlyRegionsValues, makeRegionChipData);
  }
  function makeRegionChipData(value, index) {
    return {
      key: values[index + 1],
      label: value,
      marked: false,
      id: index,
      visible: true,
    };
  }
  function getCounter() {
    return {
      marked: getMarkedCount(),
      total: totalRegions,
    };
  }

  function setVisibleClassName(region) {
    return region.marked ? classes.markedChip : classes.unmarkedChip;
  }

  const deleteTextFieldValue = (event) => {
    event.preventDefault();
    const input = textEl.current.lastElementChild.firstChild;
    input.value = '';

    setMunicipalities(
      municipalities.map((chip) => {
        chip.visible = true;

        return chip;
      })
    );
  };

  const handleChangeAll = (event) => {
    setAllMarked(event.target.checked);

    setMunicipalities(
      municipalities.map((chip) => {
        chip.marked = event.target.checked;
        return chip;
      })
    );

    setRegionCounter(getCounter());
    sendRegions(getMarkedRegions());
  };

  const handleRegionClick = (clickedRegion) => (event) => {
    event.preventDefault();

    setMunicipalities(
      municipalities.map((chip) => {
        if (clickedRegion.id == chip.id) {
          chip.marked = !chip.marked;
        }
        return chip;
      })
    );

    if (getMarkedCount() === 0) {
      setAllMarked(false);
    }

    if (getMarkedCount() === regionCounter.total) {
      setAllMarked(true);
    }
    setRegionCounter(getCounter());
    sendRegions(getMarkedRegions());
    document.getElementById('clear_focus').focus();
  };

  const findRegion = (event) => {
    const textValue = event.target.value.toLowerCase();

    setMunicipalities(
      municipalities.map((chip) => {
        chip.label.toLowerCase().includes(textValue)
          ? (chip.visible = true)
          : (chip.visible = false);

        return chip;
      })
    );
  };

  return (
    <div className={classes.root}>
      <div>
        <FormControl className={classes.textInput}>
          <TextField
            id="standard-secondary"
            label={t('Municipalities')}
            color="primary"
            onChange={findRegion}
            ref={textEl}
          />
          <HighlightOffIcon onClick={deleteTextFieldValue} color="disabled" />
        </FormControl>
        <div>
          <Checkbox
            checked={allMarked}
            color="primary"
            onChange={handleChangeAll}
            inputProps={{ 'aria-label': 'primary checkbox' }}
          />
          <span>{allMarked ? t('Unmark all') : t('Mark all')}</span>
        </div>
      </div>
      <div>
        <span>{t('Chosen')}</span>
        <span>{regionCounter.marked}</span>
        <span> | </span>
        <span>{regionCounter.total}</span>
      </div>
      <div className={classes.container}>
        {municipalities.map((region) => {
          return (
            <Zoom in={true} key={region.id}>
              <Chip
                onClick={handleRegionClick(region)}
                size="small"
                label={region.label}
                data-id={region.key}
                className={
                  region.visible
                    ? setVisibleClassName(region)
                    : classes.unvisible
                }
              />
            </Zoom>
          );
        })}
      </div>
    </div>
  );
};
