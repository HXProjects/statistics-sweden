import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import orange from '@material-ui/core/colors/orange';
import grey from '@material-ui/core/colors/grey';
import Tooltip from '@material-ui/core/Tooltip';
import * as _ from 'lodash';

const useStyles = makeStyles({
  max_value_graph: {
    backgroundColor: orange[400],
    borderRadius: '2px',
    top: '-40px',
    position: 'relative',
  },
  grid_line: {},
  label: {
    color: grey[50],
  },
  yearLabel: {
    WebkitTransform: 'rotate(-90deg)',
    color: grey[50],
    display: 'inline-block',
  },
});

export const VisualGraph = (props) => {
  const regionList = props.regionList;
  const yearsOptions = props.years;

  const classes = useStyles();

  const startX = 50;
  const startY = 250;

  const procentLabelX = 5;
  let procentLabelY = startY - 10;
  const procentLabelYDecrement = 20;

  let yearLabelX = startX - 15;
  const yearLabelY = 270;

  const yAxisCoefficient = 2;
  const xAxisIncrement = 25;
  let xAxisPoint = startX;

  const gridsLines = [
    ['1h', 30, 250, 430, 250],
    ['2h', 30, 230, 430, 230],
    ['3h', 30, 210, 430, 210],
    ['4h', 30, 190, 430, 190],
    ['5h', 30, 170, 430, 170],
    ['6h', 30, 150, 430, 150],
    ['7h', 30, 130, 430, 130],
    ['8h', 30, 110, 430, 110],
    ['9h', 30, 90, 430, 90],
    ['10h', 30, 70, 430, 70],
    ['11h', 30, 50, 430, 50],
    ['2v', 30, 30, 30, 250],
  ];

  const procentLabelPoints = [10, 20, 30, 40, 50, 60, 70, 80, 90];

  const transformToLine = (lineData) => {
    const line = (
      <Tooltip
        key={lineData.maxValue + lineData.regionsKey}
        title={lineData.maxValue + ' ' + _.join(lineData.regionsKey, '; ')}
      >
        <line
          x1={xAxisPoint + xAxisIncrement}
          y1={startY}
          x2={xAxisPoint + xAxisIncrement}
          y2={startY - Number(lineData.maxValue) * yAxisCoefficient}
          stroke={grey[50]}
          strokeWidth="12"
        />
      </Tooltip>
    );
    xAxisPoint = xAxisPoint + xAxisIncrement;
    return line;
  };

  const getLabelProcent = (n) => {
    procentLabelY = procentLabelY - procentLabelYDecrement;
    return (
      <foreignObject
        key={n}
        x={procentLabelX}
        y={procentLabelY}
        width="15"
        height="15"
        overflow="visible"
      >
        <span className={classes.label}>{n}</span>
      </foreignObject>
    );
  };

  const getLineForGrid = (lineCoord) => {
    return (
      <line
        key={lineCoord[0]}
        x1={lineCoord[1]}
        y1={lineCoord[2]}
        x2={lineCoord[3]}
        y2={lineCoord[4]}
        stroke={grey[50]}
        strokeDasharray="5,5"
      />
    );
  };

  const getLabelYear = (n) => {
    yearLabelX = yearLabelX + xAxisIncrement;
    return (
      <foreignObject
        key={n}
        x={yearLabelX}
        y={yearLabelY}
        width="15"
        height="15"
        overflow="visible"
      >
        <span className={classes.yearLabel}>{n}</span>
      </foreignObject>
    );
  };

  //<Tooltip title="Delete"></Tooltip>
  return (
    <>
      <div className={classes.max_value_graph}>
        <div>
          <svg width="500" height="300" xmlns="http://www.w3.org/2000/svg">
            <g>{gridsLines.map((lineCoord) => getLineForGrid(lineCoord))}</g>
            <g>{_.map(regionList, transformToLine)}</g>
            <g>{procentLabelPoints.map((label) => getLabelProcent(label))}</g>
            <g>{yearsOptions.map((year) => getLabelYear(year))}</g>
          </svg>
        </div>
      </div>
    </>
  );
};
