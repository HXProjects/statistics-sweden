import React from 'react';
import Card from '@material-ui/core/Card';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';

import { VisualGraph } from './visual-root';

const useStyles = makeStyles({
  card_root: {
    marginTop: 100,
    minWidth: 275,
    maxWidth: 500,
    overflow: 'visible',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});

export const MaxValueGraph = (props) => {
  const classes = useStyles();

  const listForGraph = props.regions;
  const regionCount = props.options.regions.length;
  const yearOptions = props.options.years;

  return (
    <Card className={classes.card_root}>
      <CardContent>
        <VisualGraph regionList={listForGraph} years={yearOptions} />
        <Typography variant="h5" component="h2">
          Max value for {regionCount} region{regionCount > 1 ? 's' : ''}
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          updated 1 minute ago
        </Typography>
      </CardContent>
    </Card>
  );
};
