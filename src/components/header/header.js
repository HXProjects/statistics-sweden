import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import grey from '@material-ui/core/colors/grey';

import { HeaderTabs } from './header-tabs';
import { LanguageSelector } from '../language-selector/language-selector';

const useStyles = makeStyles({
  root: {
    display: 'flex',
    justifyContent: 'space-evenly',
    backgroundColor: grey[100],
    width: '80%',
    position: 'fixed',
    marginLeft: '20%',
    top: 0,
    zIndex: '200',
  },
});

export const Header = () => {
  const classes = useStyles();

  return (
    <>
      <header className={classes.root}>
        <HeaderTabs />
        <LanguageSelector />
      </header>
    </>
  );
};
