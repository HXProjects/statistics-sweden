import React from 'react';
import { useTranslation } from 'react-i18next';
import grey from '@material-ui/core/colors/grey';
import { makeStyles } from '@material-ui/core/styles';
import CheckIcon from '@material-ui/icons/Check';
import './about.css';

const photo = '../assets/images/me.jpg';
const backgroundSectionHead = '../assets/images/gtbrg.jpg';
const mainBackground = '../assets/images/gtborgback.jpg';

const useStyles = makeStyles((theme) => ({
  main: {
    backgroundColor: grey[200],
    background: `url(${mainBackground}) no-repeat center`,
    backgroundSize: 'cover',
    //  height: '100vh',
    opacity: '1',
    '--media-height': '100vh',
    '--media-width': '100vh',
    // width: '100%',
  },
  wrapper: {
    maxWidth: '1400px',
    margin: '100px auto',
    position: 'relative',
    width: '90%',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    height: '100%',
    fontSize: '20px',
    flexWrap: 'wrap',
    '--media-height': '100vh',
    '--media-width': '100vh',
  },
  section_wrapper: {
    position: 'relative',
    display: 'flex',
    flexWrap: 'wrap',
    width: '100%',
    justifyContent: 'space-around',
  },
  section_head: {
    position: 'relative',
    fontWeight: 'bolder',
    width: '100%',
    height: '100vh',
  },
  welcome_container: {
    background: `url(${mainBackground}) no-repeat center`,
    backgroundSize: 'cover',
    width: '100%',
    height: '100vh',
    margin: '0 0',
    backgroundSize: 'cover',
  },
  head_header: {
    fontWeight: 'bolder',
    color: grey[400],
    position: 'sticky',
    top: '40px',
    left: '0',
    alignSelf: 'start',
    zIndex: '200',
  },

  img: {
    top: 0,
    width: '30%',
    clipPath: 'circle(30%)',
    opacity: '0.9',
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
  img_second: {
    top: 0,
    width: '30%',
    clipPath: 'circle(30%)',
    transition: '4.70s ease',
    transform: 'rotate(360deg)',
    opacity: '0.9',
    [theme.breakpoints.down('xs')]: {
      display: 'none',
    },
  },
  block: {
    width: '30%',
    fontSize: '22px',
    margin: '30px 0',
    alignSelf: 'center',
  },
  stabil_block: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    border: `solid 1px ${grey[50]}`,
    borderRadius: '5px',
    maxWidth: '450px',
    minWidth: '450px',
    minHeight: '250px',
    fontSize: '22px',
    alignSelf: 'center',
    padding: '20px',
    backgroundColor: grey[50],
    color: grey[800],
    margin: '10px',
    flexWrap: 'wrap',
  },
  grey: {
    color: grey[500],
    alignSelf: 'end',
  },
  background: {
    backgroundColor: grey[200],
    '--media-height': '100vh',
    '--media-width': '100vh',
  },
  list_style: {
    listStyle: 'none',
  },
  gitlab: {
    width: '45px',
    height: '45px',
    marginTop: '10px',
    marginLeft: '30px',
    marginRight: '30px',
    display: 'inline',
  },
  flex_container: {
    [theme.breakpoints.up('lg')]: {
      display: 'flex',
      justifyContent: 'space-around',
      width: '100%',
    },
  },
}));

export const About = ({ route }) => {
  const classes = useStyles();
  const { t } = useTranslation();
  const aboutSite = t('about site');
  const tecknolog = t('about technologies');
  const gitlab = t('about gitlab developing');
  const github = t('about github developing');
  const gitlaburl = 'https://gitlab.com/HXProjects/statistics-sweden';

  function handleMouseOverImg(event) {
    event.target.className = classes.img_second;
  }
  function handleMouseLeaveImg(event) {
    event.target.className = classes.img;
  }
  return (
    <>
      <div>
        <div className={classes.main}>
          <div className={classes.wrapper}>
            <h2 className="head_header">{t('Welcome!')}</h2>
            <div className={classes.grey}>
              Those who wish to sing.. always find a song.
            </div>
            <div className={classes.grey}>Swedish proverb</div>

            <div className={classes.img_second}></div>

            <div className={classes.section_head}></div>
          </div>
        </div>
        <div className={classes.wrapper}>
          <div className={classes.section_wrapper}>
            <div className={classes.block}>{aboutSite}</div>
            <img
              src={photo}
              alt="Volha"
              className={classes.img}
              onMouseOver={handleMouseOverImg}
              onMouseLeave={handleMouseLeaveImg}
            ></img>
          </div>
        </div>
        <div className={classes.background}>
          <div className={classes.wrapper}>
            <h2 className="head_header">{t('Technologies')}</h2>

            <div className="flex-container">
              <div className="transition1">
                <div className={classes.stabil_block}>
                  <ul className={classes.list_style}>
                    <li>{<CheckIcon />} React</li>
                    <li>{<CheckIcon />} Redux</li>
                    <li>{<CheckIcon />} Material-ui</li>
                    <li>{<CheckIcon />} Webpack</li>
                    <li>{<CheckIcon />} i18next</li>
                    <li>{<CheckIcon />} babel</li>
                    <li>{<CheckIcon />} jest</li>
                  </ul>
                </div>
              </div>
              <div className="transition2">
                <div className={classes.stabil_block}>{tecknolog}</div>
              </div>
              <div className="transition3">
                <div className={classes.stabil_block}>
                  {gitlab}{' '}
                  <a href={gitlaburl} className="link">
                    <img
                      className={classes.gitlab}
                      src="https://cdn.iconscout.com/icon/free/png-256/gitlab-3-555563.png"
                    ></img>{' '}
                  </a>
                </div>
              </div>
              <div className="transition4">
                <div className={classes.stabil_block}>{github}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
