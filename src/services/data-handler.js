import * as _ from 'lodash';

export const getMaxValuePerYear = (data) => {
  const sortedByYear = _.sortBy(data, [
    function (item) {
      return item.key[1];
    },
  ]);

  const maxValuesCollection = {};

  function updateMaxValuesData(item) {
    const year = item.key[1];
    const valueForCurrentRegion = item.values[0];

    if (valueForCurrentRegion == '..') {
    } else if (!maxValuesCollection.hasOwnProperty(year)) {
      createNewEntry(item);
    } else if (
      Number(maxValuesCollection[year].maxValue) < Number(valueForCurrentRegion)
    ) {
      rewriteEntry(item);
    } else if (maxValuesCollection[year].maxValue == valueForCurrentRegion) {
      addNewRegionKeyToEntry(item);
    }
  }

  function createNewEntry(source) {
    maxValuesCollection[source.key[1]] = {
      maxValue: source.values[0],
      regionsKey: [source.key[0]],
    };
  }

  function rewriteEntry(source) {
    maxValuesCollection[source.key[1]].maxValue = source.values[0];
    maxValuesCollection[source.key[1]].regionsKey = [source.key[0]];
  }

  function addNewRegionKeyToEntry(source) {
    const currentRegion = maxValuesCollection[source.key[1]].regionsKey;
    const regionKeys = _.concat(currentRegion, source.key[0]);
    maxValuesCollection[source.key[1]].regionsKey = [...regionKeys];
  }

  _.map(sortedByYear, updateMaxValuesData);

  return maxValuesCollection;
};
function findIndexOfKey(key, keysArray) {
  return _.indexOf(keysArray, key);
}
function getNameByIndex(index, namesArray) {
  return namesArray[index];
}
export const changeKeyToName = (sortedData, KeysAndNamesData) => {
  let names = [];
  if (KeysAndNamesData) {
    const res = _.map(sortedData, function (item) {
      _.map(item.regionsKey, function (key) {
        const index = findIndexOfKey(key, KeysAndNamesData.values);

        const name = getNameByIndex(index, KeysAndNamesData.valueTexts);
        const newNames = _.concat(names, [name]);
        names = [...newNames];
      });

      item.regionsKey = [...names];

      names = [];
      return item;
    });
    return res;
  }
};
