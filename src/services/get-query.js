export const getQuery=({key, time, region})=>{
const query = {
  query: [
    {
      code: 'ContentsCode',
      selection: {
        filter: 'item',
        values: [`${key}`],
      },
    },
    {
      code: 'Region',
      selection: {
        filter: 'item',
        values: region,
      },
    },
    {
      code: 'Tid',
      selection: {
        filter: 'item',
        values: time,
      },
    },
  ],
  response: {
    format: 'json',
  },
};
return query;
}