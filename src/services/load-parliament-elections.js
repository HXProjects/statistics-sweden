import { election_url } from './scb-urls';
const defaultApiQuery = {
  query: [
    {
      code: 'ContentsCode',
      selection: {
        filter: 'item',
        values: ['ME0104B8'],
      },
    },
    {
      code: 'Region',
      selection: {
        filter: 'item',
        values: ['00'],
      },
    },
    {
      code: 'Tid',
      selection: {
        filter: 'All',
        values: ['*'],
      },
    },
  ],
  response: {
    format: 'json',
  },
};

function getOptions(fetchQuery=defaultApiQuery){
  return {
  method: 'POST',
  body: JSON.stringify(fetchQuery),
  headers: {
    'Content-Type': 'text/plain',
  },
};

}
export const loadParliamentElectionResult = (opt) => {
  const query = getOptions(opt);
  const fetchResult = fetch(election_url, query);
  const result = fetchResult
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data;
    });

  return result;
};
