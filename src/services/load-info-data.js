import { election_url } from './scb-urls';

export const getAvailableYears = () => {
  const fetchResult = fetch(election_url);
  const result = fetchResult
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return data.variables[2].valueTexts;
    });

  return result;
};
export const getAvailableMunicipalies = () => {
  const fetchResult = fetch(election_url);
  const result = fetchResult
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      return {
        values: data.variables[0].values,
        valueTexts: data.variables[0].valueTexts,
      };
    });

  return result;
};
