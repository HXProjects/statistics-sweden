import * as types from './action-types';
import { loadParliamentElectionResult } from '../../services/load-parliament-elections';


import { startApiCall } from './api-status-actions';
import {
  getAvailableYears,
  getAvailableMunicipalies,
} from '../../services/load-info-data';

export function loadParliamElectionData(electionsData) {
  return {
    type: types.LOAD_PARLIAMENTARY_ELECTION_DATA_SUCCESS,
    electionsData,
  };
}

export function loadAvailableYears(availableYears) {
  return {
    type: types.LOAD_AVAILABLE_YEARS_SUCCESS,
    availableYears,
  };
}
export function loadAvailableRegions(availableRegions) {
  return {
    type: types.LOAD_AVAILABLE_REGIONS_SUCCESS,
    availableRegions,
  };
}

export function loadParliamentData(opt) {
  return function (dispatch) {
    dispatch(startApiCall());
    return loadParliamentElectionResult(opt)
      .then((elections) => {
        dispatch(loadParliamElectionData(elections));
      })
      .catch((error) => {
        throw error;
      });
  };
}
export function loadExistingYears() {
  return function (dispatch) {
    dispatch(startApiCall());
    return getAvailableYears()
      .then((years) => {
        dispatch(loadAvailableYears(years));
      })
      .catch((error) => {
        throw error;
      });
  };
}

export function loadExistingRegions() {
  return function (dispatch) {
    dispatch(startApiCall());
    return getAvailableMunicipalies()
      .then((regions) => {
        dispatch(loadAvailableRegions(regions));
      })
      .catch((error) => {
        throw error;
      });
  };
}
