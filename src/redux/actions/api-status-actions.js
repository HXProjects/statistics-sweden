import * as types from './action-types';

export function startApiCall() {
  return { type: types.START_API_CALL };
}
