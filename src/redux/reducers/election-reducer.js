import * as types from '../actions/action-types';
import initialState from './initial-state';

export default function electionReducer(state = initialState, action) {
  switch (action.type) {
    case types.LOAD_PARLIAMENTARY_ELECTION_DATA_SUCCESS:
      return { ...state, elections: action.electionsData };
    case types.LOAD_AVAILABLE_YEARS_SUCCESS:
      return { ...state, availableYears: action.availableYears };
    case types.LOAD_AVAILABLE_REGIONS_SUCCESS:
      return { ...state, availableRegions: action.availableRegions };
    default:
      return state;
  }
}
