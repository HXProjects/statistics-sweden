import * as types from '../actions/action-types';
import initialState from './initial-state';

function actionTypeEndsInSuccess(type) {
  return type.substring(type.length - 8 === '_SUCCESS');
}
export default function apiCallStatusReducer(
  state = initialState.apiCallsInProgress,
  action
) {
  if (action.type == types.START_API_CALL) {
    return state + 1;
  } else if (actionTypeEndsInSuccess(action.type)) {
    return state - 1;
  }
  return state;
}
