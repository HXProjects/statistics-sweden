import { combineReducers } from 'redux';
import elections from './election-reducer';
import apiCallsInProgress from './api-status-reducer';

const rootReducer = combineReducers({
  elections,
  apiCallsInProgress,
});

export default rootReducer;
