import { RouteConfig } from 'react-router-config';

import { App } from '../components/app/App';
import { ElectionsPage } from '../components/election-page/election-page';
import { About } from '../components/about/About';
import { NotFound } from '../components/not-found/NotFound';

const routes = [
  {
    path: '/',
    component: App,
    routes: [
      {
        path: '/',
        exact: true,
        component: ElectionsPage,
      },
      {
        path: '/about',
        component: About,
      },
      {
        component: NotFound,
      },
    ],
  },
];

export default routes;
