import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import translationEN from '../assets/i18n/en.json';
import translationSV from '../assets/i18n/sv.json';

i18n.use(initReactI18next).init({
  resources: {
    en: {
      translations: translationEN,
    },
    sv: {
      translations: translationSV,
    },
  },
  fallbackLng: 'en',
  lng: 'en',
  debug: true,

  // have a common namespace used around the full app
  ns: ['translations'],
  defaultNS: 'translations',

  interpolation: {
    escapeValue: false, // not needed for react as it escapes by default
  },
});

export default i18n;
